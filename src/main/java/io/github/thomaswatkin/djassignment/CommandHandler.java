package io.github.thomaswatkin.djassignment;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

/*
 * Handler for the four commands
 */
public class CommandHandler implements CommandExecutor {
    private final DJAssignment plugin;
    
    public CommandHandler(DJAssignment instance) {
        plugin = instance;
    }
    
    
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {    	
        /*
         * The /reload command reloads the config.yml into memory
         * Because bukkit uses the /reload command, it must be
         * addressed with /djassignment:reload 
         */
        if(cmd.getName().equalsIgnoreCase("reload")){
        	plugin.reloadConfig();
        	plugin.getLogger().info("Reload of DJAssignment Complete.");
        	return true;
        }         
        
        /*
         * The /join command assigns a new message to be sent to the
         * player when they join the server. Colors can be applied to
         * the message either with either section-sign(�), ampersand(&)
         * notation, or a mix of both. 
         * 
         * Leave args blank to remove join message.
         * 
         * eg. /join &5Welcome �aBack,&5Kiddo
         */
        else if(cmd.getName().equalsIgnoreCase("join")){
        	//Create the join message string
        	StringBuilder buffer = new StringBuilder("");
        	for(int i = 0; i < args.length; i++)
        	{
        		if(i==0) 
        			buffer.append(args[i]);
        		else
        			buffer.append(' ').append(args[i]);
        	}
        	
        	//Apply the message in memory and save to disk
        	plugin.getConfig().set("join", buffer.toString());
        	plugin.saveConfig();

        	//Remove color code before displaying on console
        	plugin.getLogger().info("Join message updated to " + ChatColor.stripColor(
        			ChatColor.translateAlternateColorCodes('&', buffer.toString())));
        	return true;
        }
        
        /*
         * The /firstjoin command assigns a new message to be sent to the
         * player when they first join the server. Colors can be applied to
         * the message either with either section-sign(�), ampersand(&)
         * notation, or a mix of both. 
         * 
         * Leave args blank to remove join message.
         * 
         * eg. /firstjoin &5Welcome �aTo &5The Server!
         */
        else if(cmd.getName().equalsIgnoreCase("firstjoin")){
        	//Create the join message string
        	StringBuilder buffer = new StringBuilder("");
        	for(int i = 0; i < args.length; i++)
        	{
        		if(i==0)
        			buffer.append(args[i]);
        		else
        			buffer.append(' ').append(args[i]);
        	}
        	
        	// Apply the message in memory and then save to disk
        	plugin.getConfig().set("firstJoin", buffer.toString());
        	plugin.saveConfig();

        	//Remove color code before displaying on console
        	plugin.getLogger().info("Join message updated to " + ChatColor.stripColor(
        			ChatColor.translateAlternateColorCodes('&', buffer.toString())));
        	return true;
        } 
        
        /*
         * The /block command sets what any block that is broken is
         * to be changed into. Block names aren't case-sensitive, and 
         * can be written either with spaces or underscores. Invalid
         * block names, or blocks that can't be placed will return failed.
         * 
         * eg. /block DIAMOND_BLOCK     or    /block diamond block
         */
        else if(cmd.getName().equalsIgnoreCase("block")){
        	if(args.length == 0){
        		return false;
        	}
        	
        	//Create the material string in the correct format
        	StringBuilder buffer = new StringBuilder(args[0].toUpperCase());
        	for(int i = 1; i < args.length; i++)
        	{
        	    buffer.append('_').append(args[i].toUpperCase());
        	}
        	
        	//Check to see if material exists
        	Material mat = Material.getMaterial(buffer.toString());
        	if(mat == null || mat.isBlock() == false){
        		return false;
        	}

        	plugin.getConfig().set("block", buffer.toString());
        	plugin.saveConfig();
        	
        	plugin.getLogger().info("Block replacement type set to " + buffer.toString());
        	return true;
        }         
        else {
        	
        	return false;
        }
        
    }
}