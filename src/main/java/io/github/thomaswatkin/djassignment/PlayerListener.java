package io.github.thomaswatkin.djassignment;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerListener implements Listener {
    private final DJAssignment plugin;

    public PlayerListener(DJAssignment instance) {
        plugin = instance;
    }

    /*
     * onPlayerJoin() handles the message sent to the player when they
     * join the server. If it's their first time they get a specialized 
     * welcome message and otherwise they get the standard greeting. The 
     * messages can accommodate built in colours with �<colour> or
     * will translate a colour encoded with ampersand(&) such as &4
     */
    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        String msg;
        
        //Get firstjoin message if player is new to the server
        if(player.hasPlayedBefore()){
        	msg = plugin.getConfig().getString("join");
        } else {
        	msg = plugin.getConfig().getString("firstJoin");
        }
        
        //Translate any ampersand coded color in the string and send to player
        if(msg != null && !msg.isEmpty()){
        	player.sendMessage(ChatColor.translateAlternateColorCodes('&', msg));
        }
        
    }

}