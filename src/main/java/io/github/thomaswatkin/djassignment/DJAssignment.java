package io.github.thomaswatkin.djassignment;

import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.PluginManager;

public final class DJAssignment extends JavaPlugin{
	private final PlayerListener playerListener = new PlayerListener(this);
	private final BlockListener blockListener = new BlockListener(this);
	
    @Override
    public void onEnable() {
    	//Save default config.yml, does nothing if config.yml exists
    	this.saveDefaultConfig();
    	
    	//Register events in the event listener classes
        PluginManager pm = getServer().getPluginManager();
        pm.registerEvents(playerListener, this);
        pm.registerEvents(blockListener, this);
        
        //Register the commands
        this.getCommand("reload").setExecutor(new CommandHandler(this));
        this.getCommand("join").setExecutor(new CommandHandler(this));
        this.getCommand("firstjoin").setExecutor(new CommandHandler(this));
        this.getCommand("block").setExecutor(new CommandHandler(this));
    }
    
    @Override
    public void onDisable() {
    }
}

