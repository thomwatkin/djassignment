package io.github.thomaswatkin.djassignment;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

public class BlockListener implements Listener{
    private final DJAssignment plugin;
	
    public BlockListener(DJAssignment instance) {
        plugin = instance;
    }
    
    /*
     * onBlockBreak() waits for a block break event. When one is
     * found it cancels the block breaking and changes the material
     * to one specified in the plugin's config.yml
     */
    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
    	//Cancel the block breaking
        event.setCancelled(true);
        
        //Get the new material
        Material mat = Material.getMaterial(plugin.getConfig().getString("block"));
        if(mat == null){
        	//Defaults the new block to stone in case the value doesn't load correctly
        	mat = Material.STONE;
            plugin.getLogger().info("Block material not properly set. Check config.yml");
        }
        
        //Change the block material
        Block bl = event.getBlock();
        bl.setType(mat);
    }
}
