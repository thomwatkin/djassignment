# README #

Code and Jar for the DoubleJump Programming Assignment

### ###

* Made using Java 7 in eclipse
* Jar can be found in the target folder, ready to go.
* the four commands can all be used from the server console or by OPs with:  
	*-djassignment:reload  
	*-join <message-text>  
	*-firstjoin <message-text>  
	*-block <block-type>  
* reload requires the plugin to be named due to the conflict with the native bukkit reload

### ####